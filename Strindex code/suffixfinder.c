#include <stdio.h>

int strindex(char c[], char s[]) {

    int i, b = 0;
    int suflen = sizeof s;

    for (i = 0; c[i] != '\0'; i++) {
        if (c[i] == s[b] && b < suflen) {
            b++;
        }
        else if (c[i] == s[b] && b == suflen) {
            return 1;
        }
        else {
            b = 0;
        }
    }
}

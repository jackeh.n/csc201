#include <stdio.h>

int main() {
    int c, nl, nt, nb;
    
    nl = 0;
    nt = 0;
    nb = 0;
    
    while ((c = getchar()) != EOF)
        if (c == '\n')
            ++nl;
        if (c == '\t')
            ++nt;
        if (c == '\b')
            ++nb;
    printf("%d\n", nl);
    printf("%d\n", nt);
    printf("%d\n", nb);
}

#include <stdio.h>

int main()
{
    int fahr, celsius;
    int lower, upper, step;
    
    lower = 0;
    upper = 300;
    step = 20;
    
    printf("Temperature Table\n");
    
    celsius = lower;
    while (fahr <= upper) { // To convert fahrenheit to celsius, I swapped the variables and reversed the formula //
        fahr = celsius * 9 / 5 + 32; 
        printf("%d\t%d\n", celsius, fahr);
        celsius = celsius + step;
    }
}

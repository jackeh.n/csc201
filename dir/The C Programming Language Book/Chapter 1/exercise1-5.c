#include <stdio.h>

int main()
{
    int fahr, celsius;
    int lower, upper, step;
    
    lower = 300;
    upper = 0;
    step = 20;
    
    printf("Temperature Table\n"); // I added this to make a heading above the table //
    
    fahr = lower;
    while (fahr >= upper) {
        celsius = 5 * (fahr-32) / 9;
        printf("%d\t%d\n", fahr, celsius);
        fahr = fahr - step;
    }
}

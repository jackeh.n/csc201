#include <stdio.h>
#define MIN 80

int main() {

    int c, v = 0, count = 0;

    while ((c = getchar()) != EOF) {
        if (count < MIN && v == 1) {
            count++;
            c++;    /* no pun intended :) */
        }
        else if (count == MIN) {}
            v = 1;
            c = 0;
            count++;
        }
        else {
            printf(c);
            c++;
        }
    }
}
